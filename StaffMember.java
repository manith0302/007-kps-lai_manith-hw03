package hw03;

abstract class StaffMember {

    protected int id;
    protected String name;
    protected String address;

    public StaffMember(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String toString(){
        return "Staff Member's ID : " + getId() + "\nStaff Member's ID : " + getName() + "\nStaff Member's ID : " + getAddress();
    }
    public abstract double pay();

    //getter and setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address.toUpperCase();
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
