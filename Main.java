package hw03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

//Class Main
class Main{

    int id;
    String name;
    String address;
    String nameErr;
    int hoursWorked;
    double salary;
    double bonus;
    double rate;

    String regex= null;
    String regexErr;
    int a=0;
    int workH = 0;
    double rateR = 0.0;
    double salaryR = 0.0;
    double bonusR = 0.0;

    int idSearch;
    String nameSearch;
    String addressSearch;
    String nameErrSearch;
    int hoursWorkedSearch;
    double salarySearch;
    double bonusSearch;
    double rateSearch;

    Scanner input = new Scanner(System.in);
    ArrayList<StaffMember> staff = new ArrayList<>();
    Volunteer employeeVol = new Volunteer(id,name,address);
    HourlyEmployee employeeHr = new HourlyEmployee(id,name,address,hoursWorked,rate);
    SalariesEmployee employeeSala = new SalariesEmployee(id,name,address,salary,bonus);
    boolean opt;
    int option, optionMember;
    String inputOpt;
    String digit = "^(?:[1-9][0-9]{0,4}(?:\\.\\d{1,2})?|100000|100000.00)$";
    String doubleRegex = "^[0-9]+([\\\\,\\\\.][0-9]+)?$";
    String doubles = "-?[0-9]+(\\.[0-9]+)?";
    String character = "[a-zA-Z-\\s]+";

    public static void main(String[] args) {

        Main point = new Main();
        Scanner input = new Scanner(System.in);
        ArrayList<StaffMember> staff = new ArrayList<>();

        StaffMember staffSala = new SalariesEmployee(1,"linda","Kampong Cham",1000.0,50.0);
        StaffMember staffVol = new Volunteer(2,"maneth","Kampong Cham");
        StaffMember staffHr = new HourlyEmployee(3,"manith","Koh Kong",8,10.0);

        staff.add(staffSala);
        staff.add(staffVol);
        staff.add(staffHr);

        boolean opt;
        int option, optionMember;
        String inputOpt;
        String digit = "^(?:[1-9][0-9]{0,4}(?:\\.\\d{1,2})?|100000|100000.00)$";
        String character = "[a-zA-Z-\\s]+";
        while (true) {

            Collections.sort(staff, new Comparator<StaffMember>() {
                @Override
                public int compare(StaffMember o1, StaffMember o2) {
                    return o1.getName().compareTo(o2.getName());
                }
            });

            for (int i=0;i<staff.size(); i++){
                System.out.println(staff.get(i));
            }
            point.enter();

            do {
                System.out.println("\n");
                System.out.println("+_______________________________________________+");
                System.out.println("            Create Employee Detail               ");
                System.out.println("|_________-------------------------_____________|");
                System.out.println("|                                               |");
                System.out.println("|1- Add Employee                                |");
                System.out.println("|2- Update Employee Info                        |");
                System.out.println("|3- Remove Employee Info                        |");
                System.out.println("|4- Exit                                        |");
                System.out.println("|_______________________________________________|");

                System.out.println();
                System.out.print("=> Choose option(1-4) : ");
                inputOpt = input.next();
                opt = inputOpt.matches(digit);
                if (!opt) System.out.println("Please Choose a Number of Options Only 1 to 6.");
            } while (!opt);
            option = Integer.parseInt(inputOpt.trim());

            switch (option) {
                case 1:
                    point.inputEmployee();
                    break;

                case 2: ;
                    point.updateEmployee();
                    break;

                case 3:
                    point.deleteEmployee();
                    break;

                case 4:
                    System.out.println("(^-^) We would like to say good bye! (^-^)");
                    System.exit(0);
                    break;
            }
        }
    }
    //Input Employee
    public void inputEmployee(){

        do {
            System.out.println("\n");
            System.out.println("+_______________________________________________+");
            System.out.println("               Kind Of Employee                  ");
            System.out.println("|_________-------------------------_____________|");
            System.out.println("|                                               |");
            System.out.println("|1- Volunteer                                   |");
            System.out.println("|2- Hour Employee                               |");
            System.out.println("|3- Salaries Employee                           |");
            System.out.println("|4- Back                                        |");
            System.out.println("|_______________________________________________|");

            System.out.println();
            System.out.print("=> Choose option(1-4) : ");
            inputOpt = input.next();
            opt = inputOpt.matches(digit);
            if (!opt) System.out.println("Please Choose a Number of Options Only 1 to 6.");
        } while (!opt);
        optionMember = Integer.parseInt(inputOpt.trim());

        switch (optionMember){
            case 1:
                inputVolunteer();
                break;
            case 2:
                inputEmployeeHr();
                break;
            case 3:
                inputEmployeeSala();
                break;
            case 4:
                break;
        }
    }

    //Input Volunteer
    public void inputVolunteer() {
        System.out.println("========== INPUT INFO =========");

        do {
            System.out.print("\nEnter Staff Member's ID : ");
            if (a == id) {
                regexErr = input.nextLine();
                regex = input.nextLine();
            }
            else
            regex = input.nextLine();

            opt = regex.matches(digit);
            if (!opt) {
                System.out.println("Please Enter only Number.");
                a += 1;
            }
        } while (!opt);
        id = Integer.valueOf(regex);
        a = id;
        employeeVol.setId(id);

        do {
            System.out.print("\nEnter Staff Member's Name : ");
            name = input.nextLine();
            employeeVol.setName(name);
            opt = name.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);
        do {
            System.out.print("\nEnter Staff Member's Address : ");
            address = input.nextLine();
            employeeVol.setAddress(address);
            opt = address.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);

        System.out.println("\n\n");
        staff.add(new Volunteer(id,name,address));
        sort();
        for (int i=0;i<staff.size(); i++){
            System.out.println(staff.get(i));
        }
    }
    //Input EmployeeHr
    public void inputEmployeeHr() {
        System.out.println("========== INPUT INFO =========");

        do {
            System.out.print("\nEnter Staff Member's ID : ");
            if (a == id) {
                regexErr = input.nextLine();
                regex = input.nextLine();
            }
            else
                regex = input.nextLine();

            opt = regex.matches(digit);
            if (!opt) {
                System.out.println("Please Enter only Number.");
                a += 1;
            }
        } while (!opt);
        id = Integer.valueOf(regex);
        a = id;
        employeeHr.setId(id);

        do {
            System.out.print("\nEnter Staff Member's Name : ");
            name = input.nextLine();
            employeeHr.setName(name);
            opt = name.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);
        do {
            System.out.print("\nEnter Staff Member's Address : ");
            address = input.nextLine();
            employeeHr.setAddress(address);
            opt = address.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);

        do {
            System.out.print("\nEnter Staff Member's Hour of Work : ");
            regex = input.nextLine();
            opt = regex.matches(digit);
            if (!opt) System.out.println("Please Enter only Number.");
        } while (!opt);
        hoursWorked = Integer.valueOf(regex);
        employeeHr.setHoursWorked(hoursWorked);

        do {
            System.out.print("\nEnter Staff Member's Rate : ");
            regex = input.nextLine();
            opt = regex.matches(doubleRegex);
            if (!opt) System.out.println("Please Enter only Number.");

        } while (!opt);
        rate = Double.valueOf(regex);
        employeeHr.setRate(rate);

        staff.add(new HourlyEmployee(id, name, address, hoursWorked, rate));
        sort();
        for (int i=0;i<staff.size(); i++){
            System.out.println(staff.get(i));
        }
    }

    //Input EmployeeHr
    public void inputEmployeeSala() {
        System.out.println("========== INPUT INFO =========");

        do {
            System.out.print("\nEnter Staff Member's ID : ");
            if (a == id) {
                regexErr = input.nextLine();
                regex = input.nextLine();
            }
            else
                regex = input.nextLine();

            opt = regex.matches(digit);
            if (!opt) {
                System.out.println("Please Enter only Number.");
                a += 1;
            }
        } while (!opt);
        id = Integer.valueOf(regex);
        a = id;
        employeeSala.setId(id);

        do {
            System.out.print("\nEnter Staff Member's Name : ");
            name = input.nextLine();
            employeeSala.setName(name);
            opt = name.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);
        do {
            System.out.print("\nEnter Staff Member's Address : ");
            address = input.nextLine();
            employeeSala.setAddress(address);
            opt = address.matches(character);
            if (!opt) System.out.println("Please Enter only Characters.");
        } while (!opt);

        do {
            System.out.print("\nEnter Staff Member's Salary : ");
            regex = input.nextLine();
            opt = regex.matches(doubleRegex);
            if (!opt) System.out.println("Please Enter only Number.");
        } while (!opt);
        salary = Double.valueOf(regex);
        employeeSala.setSalary(salary);

        do {
            System.out.print("\nEnter Staff Member's Bonus : ");
            regex = input.nextLine();
            opt = regex.matches(doubleRegex);
            if (!opt) System.out.println("Please Enter only Number.");
        } while (!opt);
        bonus = Double.valueOf(regex);
        employeeSala.setBonus(bonus);

        System.out.println("\n\n");

        staff.add(new SalariesEmployee(id, name, address, salary, bonus));
        sort();
        for (int i=0;i<staff.size(); i++){
            System.out.println(staff.get(i));
        }
    }

    //Update Employee
    public void updateEmployee(){
        System.out.println("========== UPDATE INFO =========");

        do {
            System.out.print("Enter Staff Member's ID to Update : ");
            regex = input.nextLine();
            opt = regex.matches(digit);
            if (!opt) System.out.println("Please Enter only Number.");
        } while (!opt);
        idSearch = Integer.valueOf(regex);

        int n;
        Here :
        for (n=0 ; n<staff.size(); n++){
            if (staff.get(n).id == idSearch) {

                System.out.println(staff.get(n));

                do {
                    System.out.print("\nEnter New Staff Member's Name : ");
                    nameSearch = input.nextLine();
                    staff.get(n).name = nameSearch;
                    opt = nameSearch.matches(character);
                    if (!opt) System.out.println("Please Enter only Characters.");
                } while (!opt);
                do {
                    System.out.print("\nEnter New Staff Member's Address : ");
                    addressSearch = input.nextLine();
                    staff.get(n).address = addressSearch;
                    opt = addressSearch.matches(character);
                    if (!opt) System.out.println("Please Enter only Characters.");
                } while (!opt);

                if (salary != 0.0 && staff.get(n).id == employeeSala.getId()) {

                    do {
                        System.out.print("\nEnter New Staff Member's Salary : ");
                        regex = input.nextLine();
                        opt = regex.matches(doubleRegex);
                        if (!opt) System.out.println("Please Enter only Number.");
                    } while (!opt);
                    salarySearch = Double.valueOf(regex);
                    employeeSala.setSalary(salarySearch);

                    do {
                        System.out.print("\nEnter New Staff Member's Bonus : ");
                        regex = input.nextLine();
                        opt = regex.matches(doubleRegex);
                        if (!opt) System.out.println("Please Enter only Number.");
                    } while (!opt);
                    bonusSearch = Double.valueOf(regex);
                    employeeSala.setBonus(bonusSearch);

                    staff.remove(n);
                    staff.add(new SalariesEmployee(idSearch, nameSearch, addressSearch, salarySearch, bonusSearch));
                    System.out.println("\n\n");
                    sort();
                    for (int i=0;i<staff.size(); i++){
                        System.out.println(staff.get(i));
                    }
                    break Here;

                }
                else if (rate != 0.0 && staff.get(n).id == employeeHr.getId()) {

                    do {
                        System.out.print("\nEnter New Staff Member's Hour of Work : ");
                        regex = input.nextLine();
                        opt = regex.matches(digit);
                        if (!opt) System.out.println("Please Enter only Number.");
                    } while (!opt);
                    hoursWorkedSearch = Integer.valueOf(regex);

                    do {
                        System.out.print("\nEnter New Staff Member's Rate : ");
                        regex = input.nextLine();
                        opt = regex.matches(digit);
                        if (!opt) System.out.println("Please Enter only Number.");
                    } while (!opt);
                    rateSearch = Integer.valueOf(regex);

                    staff.remove(n);
                    staff.add(new HourlyEmployee(idSearch, nameSearch, addressSearch, hoursWorkedSearch, rateSearch));
                    System.out.println("\n\n");
                    sort();
                    for (int i=0;i<staff.size(); i++){
                        System.out.println(staff.get(i));
                    }
                    break Here;

                }
            }
            else {
                continue;
            }
            System.out.println("\n\n");
            sort();
            for (int i=0;i<staff.size(); i++){
                System.out.println(staff.get(i));
            }
        }
    }

    //Delete Employee
    public void deleteEmployee(){
        System.out.println("========== Remove INFO =========");
        do {
            System.out.print("Enter Staff Member's ID to Delete : ");
            idSearch = input.nextInt();
            String idRegex  = String.valueOf(idSearch);
            opt = idRegex.matches(digit);
            if (!opt) System.out.println("Please Enter only Number.");
        } while (!opt);
        int n;
        Here :
        for (n=0 ; n<staff.size(); n++){
            if (staff.get(n).id == idSearch) {
                System.out.println();
                System.out.println(staff.get(n));
                staff.remove(n);
                System.out.println("The Data is removed successfully");
                enter();
            }
            else {
                continue;
            }
            System.out.println("\n\n");
            sort();
            for (int i=0;i<staff.size(); i++){
                System.out.println(staff.get(i));
            }
        }
    }

    //Press Enter To Continue
    public static void enter() {
        char ch = '\u0000';
        while(true)
        {
            System.out.print("Press Enter to continue... ");
            try
            {
                ch = (char)System.in.read();
                if(ch!='\u0000') break;
            }
            catch(java.io.IOException ioexception)
            {
                System.out.println(ioexception);
            }
        }
    }

    //sort
    public void sort(){
        Collections.sort(staff, new Comparator<StaffMember>() {
            @Override
            public int compare(StaffMember o1, StaffMember o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });
    }
}
