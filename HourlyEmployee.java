package hw03;

//Hourly Employee
class HourlyEmployee extends StaffMember{

    private int hoursWorked;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int hoursWorked, double rate) {
        super(id, name, address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;

    }

    public int getHoursWorked() {
        return hoursWorked;
    }

    public void setHoursWorked(int hoursWorked) {
        this.hoursWorked = hoursWorked;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String toString(){
        return "Staff Member's ID : " + getId() + "\nStaff Member's Name : " + getName() + "\nStaff Member's Address : " + getAddress() + "\nStaff Member's Hour of Work : " + getHoursWorked() + "\nStaff Member's Rate : " + getRate() + "\nStaff Member's Payment : " + pay() + "\n===================================\n";
    }
    @Override
    public double pay() {
        return hoursWorked * rate;
    }
}
