package hw03;

//Class Volunteer
class Volunteer extends StaffMember{

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    public String toString(){
        return "Staff Member's ID : " + getId() + "\nStaff Member's Name : " + getName() + "\nStaff Member's Address : " + getAddress() + "\nThank!" + "\n===================================\n";
    }

    @Override
    public double pay() {
        return 0;
    }



}
