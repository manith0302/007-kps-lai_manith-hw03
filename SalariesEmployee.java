package hw03;

//Class Salaries Employeee
class SalariesEmployee extends StaffMember{

    private double salary;
    private double bonus;

    public SalariesEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary = salary;
        this.bonus = bonus;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public String toString(){
        return "Staff Member's ID : " + getId() + "\nStaff Member's Name : " + getName() + "\nStaff Member's Address : " + getAddress() + "\nStaff Member's Salary : " + getSalary() + "\nStaff Member's Bonus : " + getBonus() + "\nStaff Member's Payment : " + pay() + "\n===================================\n";
    }
    @Override
    public double pay() {
        return salary + bonus;
    }
}
